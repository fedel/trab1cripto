* Implementar exponenciação rápida -> talvez não precise, deixar pra depois
* Implementar uma maneira de identificar o resultado correto do Rabin
* Conversão de texto para número (no algoritmo) - OK Elgamal, falta Rabin
* Geração de números primos - talvez não precise 
* "Proteger-se" do algoritmo p-1 de Pollard (não escolher p e q, tal 
que p-1 ou q-1 possuam fatores primos pequenos
    Para se proteger desse algoritmo:
        - Escolher p1 tal que p = 2p1 + 1 é primo
        - Escolher q1 tal que q = 2q1 + 1 também é primo
* Fixar o tamanho dos parâmetros em 3072 bits 
* * para isso trabalhar com vários caracteres agrupados em 1 bloco

* Gerar gerador -> prox passo

* Reorganizar código (juntar funções uteis em um lugar específico)

* Fazer orientado a objeto

* Implementar uma versão que aplique Rabin as 2 partes do Rabin
