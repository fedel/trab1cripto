
# Extedend Euclidean Algorithm
def algexteucl(a, b):
    a0 = a
    b0 = b
    t0 = 0
    t = 1
    s0 = 1
    s = 0
    q = a0 // b0
    r = a0 - q*b0
    while r > 0:
        # calculating t
        T = t0 - q*t
        t0 = t
        t = T

        # calculating s
        T = s0 - q*s
        s0 = s
        s = T

        a0 = b0
        b0 = r
        q = a0 // b0
        r = a0 - q*b0

    r = b0
    return [r,s,t]

