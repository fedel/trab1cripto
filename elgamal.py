import random
from inversemod import inversemod

# p is a large prime
# g mod p com uma ordem prima e grande
def generateKeys(p, g):
    # a receives a random number between 1 and p-1
    a = random.randint(1, p - 1)
    A = pow(g, a, p)

    return [a,A]
    

def cript(m, A, p, g):
    '''Cript function
    m is the message, a number
    A is the public key
    p is the prime'''
    k = random.randint(1, p - 1)
    
    c1 = pow(g,k,p)
    c2 = (m*pow(A, k, p)) % p

    return [c1, c2]


def decript(c1, c2, a, p):
    '''Decript the message
    c1 and c2 are the cripted message(in numbers)
    a is the private key
    p is the prime'''
    return (inversemod(pow(c1,a,p), p)*c2) % p

# TODO: verify if is the best way. There is use of space unecessary
# because of blank spaces, and probably a unecessary resources use
# beacause ord and str functions
def criptMes(mt, A, p, g):
    '''Cript a text message
    returns a text'''
    enc = ''
    for ch in range(len(mt)):
        c1, c2 = cript(ord(mt[ch]), A, p, g)
        enc += str(c1) + " " + str(c2) + " "
    # ignore the last blank space
    enc = enc[0:-1]

    return enc

def decriptMes(ct, a, p):
    '''Decript a text message
    returns the original text'''
    ctSep = ct.split(" ")
    mt = ""
    for i in range(0, len(ctSep), 2):
        c1 = int(ctSep[i])
        c2 = int(ctSep[i+1])
        m = decript(c1, c2, a, p)
        mt += chr(m)
    
    return mt
