'''The idea of this algorithm is combinate Rabin and El Gamal
for this we choose p and q primes and N = p*q
x is choosen {1..N-2}
g is a N primitive root 
h = g^x mod N 

Encryption: 
choose a y in {1..N-2)
c1, c2 = (g^y, (m.h^y)^2 mod N)


Decryption:
apply Rabin on (m.h^y)^2 mod N and finds m'
apply elgamal on (g^y, m' mod N)


'''
import rabin
import elgamal
import random
import gmpy
import utils


def randPrime(bits):
    pr = random.randint(2**(bits-2), 2**(bits-1))
    return int(gmpy.next_prime(pr))

def findPrimitiveRoot(p):
    return 878347 

def generateKeys():
#    p = 7079
#    q = 7103
#    g = 30
    p = 0
    q = 0

    # p needs to be != q
    while p == q:
        # p = 3 mod 4
        while p % 4 != 3:
            p = randPrime(3072)
        # p = 3 mod 4
        while q % 4 != 3:
            q = randPrime(3072)

    g = findPrimitiveRoot(p*q)
    a, A = elgamal.generateKeys(p*q, g)
    return [p, q, g, a, A]

def encryption(m, A, N, g, l = 100):
    ''' Encryption
    l is the bits of padding'''
    m_pad = rabin.applyPad(m, l)
    c1, c2 = elgamal.cript(m_pad, A, N, g)
    c2_ = rabin.cript(c2, N)
    return [c1, c2_]

def decryption(c1, c2_, p, q, a, l = 100):
    c2 = rabin.decript(c2_, p, q)
    c = []
    for i in range(len(c2)):
        ci = elgamal.decript(c1, c2[i], a, p*q)
        c.append(ci)

    # returns None if doesn't work the choosen
    m = rabin.chooseReturn(c,l)
    return m

def encryptionText(mt, A, N, g, l = 100, sizeMBlock = 1536):
    ''' sizeMax : size max of a block message (bits)'''
    res = ''
    for i in range(0,len(mt)*8, 1536):
        m = utils.textToInt(mt[i:i+1536], encoding='ascii')
        c = encryption(m, A, N, g, l)
        res += str(c)

    return res

def decryptionText(ct, p, q, a, l = 100):
    c = ct.split(']')
    res = ''
    # get all elements unless last (that is empty)
    for ci in c[:-1]:
        c1 = int(ci.split(',')[0][1:])
        c2 = int(ci.split(',')[1])
        m = decryption(c1, c2, p, q, a, l)
        res += utils.textFromInt(m)

    return res
