from random import randint

def mdc(a, b):
    if b != 0:
        return mdc(b, a % b)
    else:
        return a
def SS (n, iConf):
    for i in range(iConf):
        a = randint(1, n - 1)
        if (a, n) > 1:
            return False
        if not (jacobi(a, n) % n == pow(a, (num-1) // 2, num)):
            return False
    return True

def jacobi (a, n):
    if a == 0:
        if n == 1:
            return 1
        else:
            return 0
    elif a == -1:
        if n % 2 == 0:
            return 1
        else:
            return -1
    elif a == 1:
        return 1
    elif a == 2:
        if n % 8 == 1 or n % 8 == 7:
            return 1
        elif n % 8 == 3 or n % 8 == 5:
            return -1
    elif a > n:
        return (jacobi (a % n, n))
    elif a % 2 == 0:
        return (jacobi(2, n)*jacobi(a//2, n))

    else:
        if a % 4 == 3 and n % 4 == 3:
            return -1 * jacobi (n, a)
        else:
            return jacobi (n, a)


def find_prime(nBits, iConf):
    while True:
        p = randint(2**nBits-2, 2**nBits-1)
        while (p % 2 == 0):
            p = randint(2**nBits-2, 2**nBits-1)

        while (not SS(p, iConf)):
            p = randint(2**nBits-2, 2**nBits-1)
            while (p % 2 == 0):
                p = randint(2**nBits-2, 2**nBits-1)

        p = p * 2  + 1
        if SS(p, iConf):
            return p

def randPrime(bits):
    pr = randint(2**(bits-2), 2**(bits-1))
    return int(gmpy.next_prime(pr))
