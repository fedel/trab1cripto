from algexteucl import algexteucl

def inversemod(a, p):
    '''Calculate inverse a**-1 (mod p)'''
    r, s, t = algexteucl(a, p)
    return s % p
