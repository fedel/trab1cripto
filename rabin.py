from algexteucl import algexteucl
from inversemod import inversemod

def chinRemThe(a1, a2, m1, m2):
    '''Chinese remainder theorem for 2 congruencesi
    m1 and m2 need to co-prime
    x = a1 mod m1
    x = a2 mod m2'''
    b = (a2 - a1) % m2
    y = (inversemod(m1, m2) * b) % m2
    result = (a1 + m1*y) % (m1*m2)
    return result
def sqrRtMod(x, p):
    '''Square Root Modular for p prime
    p needs to satisfy the congruence p = 3 (mod 4)
    Doesnt verify if exist the squarte root'''
    result = pow(x, (p + 1)//4, p)
 
    return result
# TODO: implement this function, or get this numbers
def generateKeys():
    # p and q has to be k/2 bits
    # and p, q = 3 (mod 4)
    p = 7079
    q = 7103
    return [p, q]

def cript(m, N):
    '''cript a number'''
    c = pow(m, 2, N)
    return c
def decript(c, p, q):
    '''decript a number'''
    # calcular c^(1/2) mod N
    # a) calcular c^(1/2) mod p
    # b) calcular c^(1/2) mod q
    # c) "juntar com o teorema chinês do resto"
    a = sqrRtMod(c, p)
    b = sqrRtMod(c, q)
    m1 = chinRemThe(a, b, p, q)
    m2 = chinRemThe(-a, -b, p, q)
    m3 = chinRemThe(-a, b, p, q)
    m4 = chinRemThe(a,-b, p, q)

    return [m1, m2, m3, m4]

def applyPad(m, l):
    '''Apply add l least bits 1 of m'''
    return (2**l)*m + ((2**l)-1)

def chooseReturn(r, l):
    '''Choose the right return of decript
    r is a array of 4 results
    l is the length of padding'''
    countRes = 0
    pad = 2**l - 1
    res = None
    for i in range(len(r)):
        # verify if the least bits of r[i] are pad
        if r[i] & pad == pad:
            countRes+=1
            # store result removing pad
            res = r[i]//2**l
        # if more than 1 answer with pad, error
        if countRes > 1:
            return None 
    return res

def criptMes(N, mt, l):
    '''Cript a text message
    N public key
    mt text message
    l size of padding'''
    enc = ''
    for ch in range(len(mt)):
        # Convert to ascii number and apply pad
        mPad = applyPad(ord(mt[ch]), l)
        enc += str(cript(mPad, N)) + " "

    # return the string with spaces between numbers
    return enc[0:-1]

def decriptMes(ct, p, q, l):
    '''Decript a text message
    l is the size of padding'''
    ctStep = ct.split(" ")
    mt = ""
    for i in range(len(ctStep)):
        cPad = int(ctStep[i])
        r = decript(cPad, p, q)
        c = chooseReturn(r, l)
        if c == None:
            mt +="*"
        else:
            mt += chr(c)

    return mt

if __name__ == "__main__":
    p, q = generateKeys()
    c = cript(11, p*q)
    print(decript(c, p, q))
