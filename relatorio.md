﻿# Relatório Trabalho 1 Algoritmos Criptograficos (MO442)

## Aluno

Gabriel de Souza Fedel - 89037

## Disponível em

https://gitlab.com/fedel/trab1cripto

## Objetivo

O objetivo do trabalho e implementar suporte tanto  cifração quanto  decifração
da combinação entre Rabin e ElGamal, de forma que o criptossistema resultante seja consistente. Ou seja, a decifração deve entregar exatamente o texto claro processado pelo algoritmo de cifração.

# Nome do algoritmo desenvolvido

ElRabinGamal2

## Experiência

Desenvolver esse trabalho foi muito interessante para conseguir revisar e
compreender melhor o funcionamento dos algoritmos Rabin e ElGamal (assim como
todos os conceitos envolvidos: aritmética modular, números primos, teorema
chinês do esto entre outros). 
A experiência foi boa por também permitir pesquisar artigos e referências sobre
esses algoritmos, e sobre as limitações deles.

O processo de desenvolvimento desse trabalho seguiu o seguinte fluxo resumido:

* implementar Rabin e El Gamal tradicional simplificado

* implementar uma maneira e escolher a mensagem correta do rabin

* implementar a conversão de texto para blocos que serão criptografados

* unir Rabin e El Gamal


Apesar da linguagem escolhida (Python) talvez não ser a melhor para questão de
desempenho, o seu uso permitiu focar em questões de algoritmo e ter menos
preocupações com questões de implmentação.

## Ideia do algoritmo

Combinar Rabin e Elgamal

* Geração de Chaves

p e q primos e p (congruente) 3 (mod 4), q (congruente) 3 (mod 4)
`N = p*q`
a = rand(1, N-1)
A = g^a mod N
g é gerador em N com ordem prima grande

Chaves públicas: A, N, g
Chaves privadas: p, q e a

* Encriptação

Trata-se de um cascateamento "parcial": primeiro encripta-se a mensagem usando ElGamal com chave pública N (diferente da versão original que utiliza um número primo) e depois encripta-se a 2a parte da mensagem encriptada (c2) utilizando Rabin 

`c1, c2 = elgamal.crypt(m, A, N, g)`

`c2' = c2^2 mod N`

mensagem encriptada: [c1,c2']


* Desencriptação:

O processo reverso é realizado desencriptando-se a 2a parte da mensagem com
Rabin:

`c2 = rabin.decrytp(c2', p, q)`

e então, aplica-se o el gamal

`m = elgamal.decrypt(c1, c2, a, N)`

(note que o elgamal precisa ser aplicado 4 vezes, visto que o resultado do
rabin são 4 raízes)

## Observações

* Utiliza-se a chave N no El Gamal para que todos os cálculos possam ser
  realizados mod N

* É aplicado um padding antes de se aplicar o El Gamal (ele será utilizado
  posteriormente para escolher o resultado correto)

## Impacto no desempenho do Rabin tradicional:

* El Gamal: utiliza 2 exponenciações na encriptação (mas elas são independentes
  da mensagem e podem ser calculadas anteriormente). Utiliza 1 exponenciação na
  desencriptação

* Rabin: para encriptar utiliza um quadrado modular. Para desencriptar
  utiliza-se o Teorema Chinês do Resto (4 vezes) com 2 exponenciações
  modulares.

A combinação dos algoritmos trará um pequeno impacto de execução no Rabin, pois irá ser aplicado no espaço de mensagems criptografadas pelo ElGamal. Também ocupará o dobro do espaço. 
Além disso na desencriptação será utilizado 1 exponenciação a mais.

## Ambiguidade no processo de decifração

O Rabin (e o ElRabinGamal2) geram 4 resultados , e existem materiais que
indicam maneiras de se escolher qual dos 4 resultados é o correto. Baseando-se
em [1] optou-se por trabalhar com um padding de 1's. Essa solução tem uma
probabilidade de não encontrar a resposta correta, porém é uma possibilidade
pequena (1/(2^l - 1)). Por padrão deixou-se definido l = 100.

O padding de 1's foi adicionado a mensagem antes do El Gamal ser aplicado

## Resiliência a ataques de texto claro escolhido

Rabin é suscetível a ataques de texto claro em um espaço de mensagem pequeno,
pois o atacante pode gerar todas as mensagens possíveis e descobrir qual a
correta

Nessa variante, como o Rabin é aplicado sobre parte da mensagem criptografada
por El Gamal, o espaço que o atacante terá que aplicar o texto claro escolhido
é um espaço gerado pela chave efêmera do El Gamal (desconhecida pelo atacante).


## Premissas de Segurança / Dificuldade de problemas

Basea-se na fatoração de inteiros (na parte Rabin), e no logaritmo de inteiros
(na parte El Gamal)

Da maneira como essa solução é apresentada, o atacante precisa desencriptar a
segunda parte da mensagem encriptada (que é similiar ao problema de fatoração
de inteiros grandes), e após isso desencriptar o Elgamal que tem a dificuldade
de logarítmo modular.

## Modo de Uso

No prompt do python:

`p, g, a, A = elrabingamal2.generateKeys()`
`N = p*g`

`# c é uma string e pode ser salvo em um arquivo`
`c = elrabingamal2.encryptionText("Aqui vai o texto", A, N, g)`

`# para desencriptar`

`m = elrabingamal2.decryptionText(c, p, q, a)`

## Pontos em aberto

* Não foi verificado se o fato de utilizar ElGamal com um número composto deixa
  ele mais fácil de ser atacado

* A aplicação do padding antes do ElGamal também pode representar uma brecha
  para facilitar a decriptação

* Organizar o código orientado a objeto

* Implementar a função que gera g

* Implementar testes automatizados


[1] GALBRAITH, Steven D. Mathematics of public key cryptography. Cambridge University Press, 2012. (Cap 24)
