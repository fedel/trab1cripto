def textToInt(text, encoding='utf-8', errors='surrogatepass'):
    return int.from_bytes(text.encode(encoding, errors), 'big')

def textFromInt(n, encoding='utf-8', errors='surrogatepass'):
    return n.to_bytes((n.bit_length() + 7) // 8, 'big').decode(encoding, errors) or '\0'

